# android_vendor_xiaomi_vili-firmware

Firmware images for Mi 11T Pro (vili), to include in custom ROM builds.

**Current version**: fw_vili_miui_VILIEEAGlobal_V14.0.10.0.TKDEUXM_c28a88196e_13.0.zip

### How to use?

1. Clone this repo to `vendor/xiaomi/vili-firmware`

2. Include it from `BoardConfig.mk` in device tree:

```
# Firmware
-include vendor/xiaomi/vili-firmware/BoardConfigVendor.mk
```
